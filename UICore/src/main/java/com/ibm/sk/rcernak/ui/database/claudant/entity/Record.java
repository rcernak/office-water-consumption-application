/*
package com.ibm.sk.rcernak.ui.database.claudant.entity;

import java.util.Objects;

public class Record {

	private String date;
	private String creatorId;
	private String tagId;
	private String userRegisteredDeviceName;
	private String volume;

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getUserRegisteredDeviceName() {
		return userRegisteredDeviceName;
	}

	public void setUserRegisteredDeviceName(String userRegisteredDeviceName) {
		this.userRegisteredDeviceName = userRegisteredDeviceName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Record))
			return false;
		Record record = (Record) o;
		return Objects.equals(date, record.date) && Objects.equals(creatorId, record.creatorId) && Objects.equals(tagId, record.tagId)
				&& Objects.equals(userRegisteredDeviceName, record.userRegisteredDeviceName) && Objects.equals(volume, record.volume);
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, creatorId, tagId, userRegisteredDeviceName, volume);
	}
}
*/
