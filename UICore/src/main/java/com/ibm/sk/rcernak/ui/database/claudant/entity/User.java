/*
package com.ibm.sk.rcernak.ui.database.claudant.entity;

import java.util.List;
import java.util.Objects;

public class User {
	private String _id;
	private String _rev;
	private String fname;
	private String lname;
	private List<Device> devices;
	private String login;
	private String password;

	public User() {
	}

	public User(final String login, final String password, final String fname, final String lname, final List<Device> devices) {
		this.login = login;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.devices = devices;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return _id;
	}

	public void setId(String id) {
		this._id = id;
	}

	public String getRev() {
		return _rev;
	}

	public void setRev(String rev) {
		this._rev = rev;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof User))
			return false;
		User user = (User) o;
		return Objects.equals(_id, user._id) && Objects.equals(_rev, user._rev) && Objects.equals(fname, user.fname) && Objects
				.equals(lname, user.lname) && Objects.equals(devices, user.devices) && Objects.equals(login, user.login) && Objects
				.equals(password, user.password);
	}

	@Override
	public int hashCode() {
		return Objects.hash(_id, _rev, fname, lname, devices, login, password);
	}
}*/
