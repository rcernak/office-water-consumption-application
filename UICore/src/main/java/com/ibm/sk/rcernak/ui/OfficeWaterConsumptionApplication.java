package com.ibm.sk.rcernak.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class OfficeWaterConsumptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfficeWaterConsumptionApplication.class, args);
	}
}
