/*
package com.ibm.sk.rcernak.ui.service;

import static com.ibm.sk.rcernak.ui.database.claudant.CloudantAccessor.DB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
//import com.ibm.sk.rcernak.ui.controller.DeprecatedUserController;
import com.ibm.sk.rcernak.ui.database.claudant.entity.ClaudantRecord;
import com.ibm.sk.rcernak.ui.database.claudant.entity.Device;
import com.ibm.sk.rcernak.ui.database.claudant.entity.Record;
import com.ibm.sk.rcernak.ui.database.claudant.entity.User;

*/
/**
 * CRUD operations using Claudant database
 *//*

@Service
public class ClaudantRecordService {

	public static final int DELAY = 1000;

	public List<Record> getRecords() throws IOException {
		final List<Record> result = new ArrayList<>();
		List<String> allDocIds = DB.getRecords().getAllDocsRequestBuilder().build().getResponse().getDocIds();
		for (String id : allDocIds) {
			final ClaudantRecord claudantRecord = DB.getRecords().find(ClaudantRecord.class, id);
			result.add(parseSimplifiedRecordFrom(claudantRecord));
		}
		return result;
	}

	public List<Record> getUserDevicesRecords(String userId) throws InterruptedException, IOException {
		final Map<String, Device> devicesMap = getUserDeviceMap(userId);

		final List<Record> result = new ArrayList<>();
		List<String> allDocIds = DB.getRecords().getAllDocsRequestBuilder().build().getResponse().getDocIds();
		for (String id : allDocIds) {
			final ClaudantRecord claudantRecord = DB.getRecords().find(ClaudantRecord.class, id);
			final String tagId = parseTagId(claudantRecord);
			if(devicesMap.keySet().contains(tagId)) {
				final Record record = parseSimplifiedRecordFrom(claudantRecord);
				record.setUserRegisteredDeviceName(devicesMap.get(tagId).getName());
				record.setVolume(devicesMap.get(tagId).getVolume());
				result.add(record);
			}
		}
		Thread.sleep(DELAY);
		return result;
	}

	public Double getUserTotal(String userId) throws IOException, InterruptedException {
		double result = 0;
		final Map<String, Device> devicesMap = getUserDeviceMap(userId);

		List<String> allDocIds = DB.getRecords().getAllDocsRequestBuilder().build().getResponse().getDocIds();
		for (String id : allDocIds) {
			final ClaudantRecord claudantRecord = DB.getRecords().find(ClaudantRecord.class, id);
			final String tagId = parseTagId(claudantRecord);
			if(devicesMap.keySet().contains(tagId)) {
				double volume = validateVolume(devicesMap.get(tagId).getVolume());
				result += volume;
			}
		}
		Thread.sleep(DELAY);
		return result;
	}

	public List<Device> getUserDevices( String userId) throws IOException, InterruptedException {
		final List<Device> result = new ArrayList<>();

		final Map<String, Device> devicesMap = getUserDeviceMap(userId);
		if(devicesMap.isEmpty()) {
			return result;
		}

		List<String> allDocIds = DB.getRecords().getAllDocsRequestBuilder().build().getResponse().getDocIds();
		for (String id : allDocIds) {
			final ClaudantRecord claudantRecord = DB.getRecords().find(ClaudantRecord.class, id);
			final String tagId = parseTagId(claudantRecord);
			if(devicesMap.keySet().contains(tagId)) {
				//user has device with the same id as in record
				final Device device = devicesMap.get(tagId);
				List<Record> records = device.getRecords();
				if(records==null) {
					records = new ArrayList<>();
				}
				records.add(parseSimplifiedRecordFrom(claudantRecord));
				device.setRecords(records);
				devicesMap.put(tagId, device);
			}
		}
		result.addAll(devicesMap.values());
		try {
			Thread.sleep(DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}

	private Record parseSimplifiedRecordFrom(ClaudantRecord claudantRecord) {
		final Record record = new Record();

		record.setDate(claudantRecord.getDate().toString());
		record.setCreatorId(claudantRecord.getDeviceId());
		record.setTagId(parseTagId(claudantRecord));

		return record;
	}

	private String parseTagId(ClaudantRecord claudantRecord) {
		final JsonParser parser = new JsonParser();
		final JsonElement element = parser.parse(claudantRecord.getData());
		final JsonObject json = element.getAsJsonObject();
		final JsonElement jsonTagId = json.get("tagId");
		return jsonTagId.getAsString();
	}

	private double validateVolume(String volume) {
		if(volume == null) {
			return 0;
		}
		if(volume.contains(",")) {
			volume = volume.replaceAll(",", ".");
		}
		return Double.valueOf(volume);
	}

*/
/*	private Map<String, Device> getUserDeviceMap(String userId) throws InterruptedException {
		final User user = new DeprecatedUserController().getUser(userId);
		final List<Device> devices = user.getDevices();
		Thread.sleep(DELAY);
		return devices == null ? new HashMap<>() : devices.stream().collect(Collectors.toMap(Device::getId, v -> v));
	}*//*

}
*/
