/*
package com.ibm.sk.rcernak.ui.database.claudant.entity;

import java.util.List;
import java.util.Objects;

public class Device {

	private String id;
	private String name;
	private String volume;
	private List<Record> records;

	public List<Record> getRecords() {
		return records;
	}

	public void setRecords(List<Record> records) {
		this.records = records;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Device))
			return false;
		Device device = (Device) o;
		return Objects.equals(id, device.id) && Objects.equals(name, device.name) && Objects.equals(volume, device.volume) && Objects
				.equals(records, device.records);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, volume, records);
	}
}
*/
