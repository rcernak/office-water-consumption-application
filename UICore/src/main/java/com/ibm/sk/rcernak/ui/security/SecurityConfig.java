/*
package com.ibm.sk.rcernak.ui.security;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
				.authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**")
				.permitAll().anyRequest().authenticated().and().httpBasic();
	}

	//TODO hash password
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(new AuthenticationProvider() {
			@Override
			public Authentication authenticate(Authentication authentication) throws AuthenticationException {
				String login = authentication.getName();
				String password = authentication.getCredentials().toString();

				final UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
						login, password, new ArrayList<>());
				if (AuthenticationCache.INSTANCE.contains(login,password)) {
					return authenticationToken;
				}

//				if (CloudantAccessor.DB.isAuthenticatedFromClaudant(login, password)) {
//					AuthenticationCache.INSTANCE.add(login, password);
//					return authenticationToken;
//				}
				//TODO next line should be "return null;" now available only for development purposes
				return authenticationToken;
			}

			@Override
			public boolean supports(Class<?> authentication) {
				return authentication.equals(UsernamePasswordAuthenticationToken.class);

			}
		});
		//auth.inMemoryAuthentication().withUser("tester").password("{noop}password").roles("USER");
	}
}
*/
