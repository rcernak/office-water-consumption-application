/*
package com.ibm.sk.rcernak.ui.database.claudant.entity;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

public class ClaudantRecord {

	private String _id;
	private String _rev;
	private String id;
	private String deviceId;
	private String deviceType;
	private String event;
	private String data;
	private String format;
	private LocalDateTime date;
	private String[] rawPayload;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_rev() {
		return _rev;
	}

	public void set_rev(String _rev) {
		this._rev = _rev;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String[] getRawPayload() {
		return rawPayload;
	}

	public void setRawPayload(String[] rawPayload) {
		this.rawPayload = rawPayload;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ClaudantRecord))
			return false;
		ClaudantRecord claudantRecord = (ClaudantRecord) o;
		return Objects.equals(_id, claudantRecord._id) && Objects.equals(_rev, claudantRecord._rev) && Objects.equals(id, claudantRecord.id) && Objects
				.equals(deviceId, claudantRecord.deviceId) && Objects.equals(deviceType, claudantRecord.deviceType) && Objects.equals(event, claudantRecord.event)
				&& Objects.equals(data, claudantRecord.data) && Objects.equals(format, claudantRecord.format) && Objects.equals(date, claudantRecord.date) && Arrays
				.equals(rawPayload, claudantRecord.rawPayload);
	}

	@Override
	public int hashCode() {
		int result = Objects.hash(_id, _rev, id, deviceId, deviceType, event, data, format, date);
		result = 31 * result + Arrays.hashCode(rawPayload);
		return result;
	}
}*/
