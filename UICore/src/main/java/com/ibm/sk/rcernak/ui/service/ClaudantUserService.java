/*
package com.ibm.sk.rcernak.ui.service;

import static com.cloudant.client.api.query.Expression.eq;
import static com.ibm.sk.rcernak.ui.database.claudant.CloudantAccessor.DB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudant.client.api.model.Response;
import com.cloudant.client.api.query.QueryBuilder;
import com.cloudant.client.api.query.QueryResult;
import com.ibm.sk.rcernak.ui.database.claudant.entity.User;

*/
/**
 * CRUD operations using Claudant database
 *//*

@Service
public class ClaudantUserService {

	//CREATE
	public boolean createUser(User user) {
		final Response saveResp = DB.getUsers().save(user);
		user.setId(saveResp.getId());
		user.setRev(saveResp.getRev());
		final Response updateResp = DB.getUsers().update(user);
		return DB.isCreateSuccessful(updateResp);
	}

	//READ
	public User getUserByLogin(String login) {
		final QueryResult<User> queryResult = DB.getUsers().query(new QueryBuilder(eq("login", login)).build(), User.class);
		return queryResult.getDocs().get(0);
	}

	public User getUser(String id) {
		return DB.getUsers().find(User.class, id);
	}

	public List<User> getUsers() throws IOException {
		final ArrayList<User> result = new ArrayList<>();
		List<String> allDocIds = DB.getUsers().getAllDocsRequestBuilder().build().getResponse().getDocIds();
		for (String id : allDocIds) {
			final User user = DB.getUsers().find(User.class, id);
			user.setPassword(null);
			result.add(user);
		}
		return result;
	}

	//UPDATE
	public boolean updateUser(User updatedUser) {
		final Response saveResp = DB.getUsers().update(updatedUser);
		return DB.isCreateSuccessful(saveResp);
	}

	//DELETE
	public boolean deleteUser(String id) {
		final User userToDelete = DB.getUsers().find(User.class, id);
		final Response removeResp = DB.getUsers().remove(userToDelete);
		return DB.isRequestSuccessful(removeResp);
	}

}
*/
