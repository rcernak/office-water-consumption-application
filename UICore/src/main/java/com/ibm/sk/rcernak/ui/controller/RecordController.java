/*
package com.ibm.sk.rcernak.ui.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.sk.rcernak.ui.database.claudant.entity.Device;
import com.ibm.sk.rcernak.ui.database.claudant.entity.Record;
import com.ibm.sk.rcernak.ui.service.ClaudantRecordService;

@RestController
@RequestMapping("/api")
@CrossOrigin()
public class RecordController {

	@Autowired
	private ClaudantRecordService recordService;

	@GetMapping("/records")
	public List<Record> getRecords() throws IOException {
		return recordService.getRecords();
	}

	//testuser id: d9292bdc436841768f7b2a930149c282
	@GetMapping("/userdevicesrecords/{userId}")
	public List<Record> getUserDevicesRecords(@PathVariable String userId) throws IOException, InterruptedException {
		return recordService.getUserDevicesRecords(userId);
	}

	@GetMapping("/usertotal/{userId}")
	public Double getUserTotal(@PathVariable String userId) throws IOException, InterruptedException {
		return recordService.getUserTotal(userId);
	}

	@GetMapping("/userdevices/{userId}")
	public List<Device> getUserDevices(@PathVariable String userId) throws IOException, InterruptedException {
		return recordService.getUserDevices(userId);
	}

}
*/
