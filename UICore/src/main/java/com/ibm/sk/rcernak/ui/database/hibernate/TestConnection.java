/*
package com.ibm.sk.rcernak.ui.database.hibernate;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.ibm.sk.rcernak.ui.database.hibernate.entity.Device;
import com.ibm.sk.rcernak.ui.database.hibernate.entity.Record;
import com.ibm.sk.rcernak.ui.database.hibernate.entity.User;

public class TestConnection {

	private static SessionFactory factory;
	private static ServiceRegistry serviceRegistry;

	public static void main(String[] args) {
		Configuration config = new Configuration();
		config.configure();
		config.addAnnotatedClass(User.class);
		config.addAnnotatedClass(Device.class);
		config.addAnnotatedClass(Record.class);
		config.addResource("User.hbm.xml");
		config.addResource("Device.hbm.xml");
		config.addResource("Record.hbm.xml");
		serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
		factory = config.buildSessionFactory(serviceRegistry);

		TestConnection hbTest = new TestConnection();
		final UUID userId1 = hbTest.insertUser("Mark", "Johnson", "mark.johnson", "mjohnson");
		final UUID userId2 = hbTest.insertUser("Samuel", "Johnson", "sam.johnson", "sjohnson");
		final UUID tagId = UUID.randomUUID();
		hbTest.insertDevice("mug",1.0, tagId, userId1);
		hbTest.insertDevice("flask",1.5, null, userId2);
		hbTest.insertRecord(java.sql.Date.valueOf(LocalDate.now()), tagId);
		hbTest.insertRecord(java.sql.Date.valueOf(LocalDate.now()), tagId);

		List<User> users = hbTest.listUsers();
		for(User u : users) {
			System.out.print(u.getUserId() + " ");
			System.out.print(u.getFirstName() + " ");
			System.out.print(u.getLastName() + " ");
			System.out.print(u.getLogin() + " ");
			System.out.print(u.getPassword() + " ");
			System.out.println();
		}

		final List<Device> devices = hbTest.listDevices();
		for (Device d : devices) {
			System.out.print(d.getDeviceId() + " ");
			System.out.print(d.getName() + " ");
			System.out.print(d.getVolume() + " ");
			System.out.print(d.getTagId() + " ");
			System.out.print(d.getUserId() + " ");
			System.out.println();
		}

		final List<Record> records = hbTest.listRecords();
		for (Record r : records) {
			System.out.print(r.getRecordId() + " ");
			System.out.print(r.getDate() + " ");
			System.out.print(r.getTagId() + " ");
			System.out.println();
		}

	}

	private UUID insertRecord(Date date, UUID tagId) {
		Session session = factory.openSession();
		Transaction tx = null;
		UUID recordIdSaved = null;
		try {
			tx = session.beginTransaction();
			Record r = new Record();
			r.setDate(date);
			r.setTagId(tagId);
			recordIdSaved = (UUID) session.save(r);
			tx.commit();
		} catch(HibernateException ex) {
			if(tx != null)
				tx.rollback();
			ex.printStackTrace();
		} finally {
			session.close();
		}

		return recordIdSaved;
	}

	private UUID insertUser(String firstName, String lastName, String login, String password) {
		Session session = factory.openSession();
		Transaction tx = null;
		UUID userIdSaved = null;
		try {
			tx = session.beginTransaction();
			User u = new User();
			u.setFirstName(firstName);
			u.setLastName(lastName);
			u.setLogin(login);
			u.setPassword(password);
			userIdSaved = (UUID) session.save(u);
			tx.commit();
		} catch(HibernateException ex) {
			if(tx != null)
				tx.rollback();
			ex.printStackTrace();
		} finally {
			session.close();
		}

		return userIdSaved;
	}

	private UUID insertDevice(String deviceName, Double volume, UUID tagId, UUID userId) {
		Session session = factory.openSession();
		Transaction tx = null;
		UUID savedDeviceId = null;
		try {
			tx = session.beginTransaction();
			Device d = new Device();
			d.setName(deviceName);
			d.setVolume(volume);
			d.setTagId(tagId);
			d.setUserId(userId);
			savedDeviceId = (UUID) session.save(d);
			tx.commit();
		} catch (HibernateException ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return savedDeviceId;
	}

	private List<User> listUsers() {
		Session sesn = factory.openSession();
		Transaction tx = null;
		List<User> users = new ArrayList<>();
		try{
			tx = sesn.beginTransaction();
			users = sesn.createQuery("From User").list();
			tx.commit();
		} catch(HibernateException e) {
			e.printStackTrace();
		} finally {
			sesn.close();
		}
		return users;
	}

	private List<Device> listDevices() {
		Session sesn = factory.openSession();
		Transaction tx = null;
		List<Device> devices = new ArrayList<>();
		try{
			tx = sesn.beginTransaction();
			devices = sesn.createQuery("From Device").list();
			tx.commit();
		} catch(HibernateException e) {
			e.printStackTrace();
		} finally {
			sesn.close();
		}
		return devices;
	}

	private List<Record> listRecords() {
		Session sesn = factory.openSession();
		Transaction tx = null;
		List<Record> records = new ArrayList<>();
		try{
			tx = sesn.beginTransaction();
			records = sesn.createQuery("From Record").list();
			tx.commit();
		} catch(HibernateException e) {
			e.printStackTrace();
		} finally {
			sesn.close();
		}
		return records;
	}
}*/
