/*
package com.ibm.sk.rcernak.ui.database.claudant;

import static com.cloudant.client.api.query.Expression.eq;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.cloudant.client.api.model.Response;
import com.cloudant.client.api.query.QueryBuilder;
import com.cloudant.client.api.query.QueryResult;
import com.ibm.sk.rcernak.ui.database.claudant.entity.User;

public enum CloudantAccessor {
	DB;

	private static final String USERS_DATABASE = "users_nosql_db";
	private static final String RECORDS_DATABASE = "raspberry_nosql_db";
	private static final int SUCCESSFUL_RESPONSE = 200;
	private static final int SUCCESSFUL_CREATE = 201;

	private Database usersDb;
	private Database recordsDb;
	private CloudantClient cloudantDb;

	@Component
	public static class CloudantInjector {
		@Autowired private CloudantClient cloudantDb;

		@PostConstruct
		public void postConstruct() {
			CloudantAccessor.DB.cloudantDb = this.cloudantDb;
		}
	}

	public Database getUsers() {
		if(usersDb==null) {
			usersDb = cloudantDb.database(USERS_DATABASE, false);
		}
		return usersDb;
	}

	public Database getRecords() {
		if(recordsDb==null) {
			recordsDb = cloudantDb.database(RECORDS_DATABASE, false);
		}
		return recordsDb;
	}

	public boolean isRequestSuccessful(Response resp) {
		return resp.getStatusCode() == SUCCESSFUL_RESPONSE;
	}

	public boolean isCreateSuccessful(Response resp) {
		return resp.getStatusCode() == SUCCESSFUL_CREATE;
	}

	public boolean isAuthenticatedFromClaudant(final String login, final String password) {
		final Database usersDb = CloudantAccessor.DB.getUsers();
		final QueryResult<User> queryResult = usersDb.query(new QueryBuilder(eq("login", login)).build(), User.class);
		final List<User> users = queryResult.getDocs();

		//TODO remove and change to cached authentication to spare number of DB accesses
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (password!=null && users!= null
				&& users.stream().anyMatch(e -> password.equals(e.getPassword()))) {
			return true;
		} else {
			return false;
		}
	}
}
*/
