/*
package com.ibm.sk.rcernak.ui.database.hibernate.entity;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Device", uniqueConstraints = { @UniqueConstraint(columnNames = "DEVICE_ID") })
public class Device implements Serializable {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "DEVICE_ID", unique = true, nullable = false)
	private UUID deviceId;

	@Column(name = "NAME", unique = true, nullable = false, length = 100)
	private String name;

	@Column(name = "VOLUME", unique = false, nullable = false)
	private Double volume;

	@Column(name = "TAG_ID", unique = false, nullable = true)
	private UUID tagId;

	@Column(name = "USER_ID", unique = false, nullable = true)
	private UUID userId;

	public UUID getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(UUID deviceId) {
		this.deviceId = deviceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public UUID getTagId() {
		return tagId;
	}

	public void setTagId(UUID tagId) {
		this.tagId = tagId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Device))
			return false;
		Device device = (Device) o;
		return Objects.equals(deviceId, device.deviceId) && Objects.equals(name, device.name) && Objects.equals(volume, device.volume)
				&& Objects.equals(tagId, device.tagId) && Objects.equals(userId, device.userId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(deviceId, name, volume, tagId, userId);
	}
}
*/
