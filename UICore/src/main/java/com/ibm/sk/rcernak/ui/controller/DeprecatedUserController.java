/*
package com.ibm.sk.rcernak.ui.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.sk.rcernak.ui.database.claudant.entity.User;
import com.ibm.sk.rcernak.ui.service.ClaudantUserService;

@Deprecated
@RestController
@RequestMapping("/api")
@CrossOrigin()
public class DeprecatedUserController {

	@Autowired
	private ClaudantUserService userService;

	@PostMapping("/user")
	public boolean createUser(@RequestBody User user) {
		return userService.createUser(user);
	}

	@GetMapping("/users")
	public List<User> getUsers() throws IOException {
		return userService.getUsers();
	}

	@GetMapping("/user/{id}")
	public User getUser(@PathVariable String id) {
		return userService.getUser(id);
	}

	@GetMapping("/user/login/{login}")
	public User getUserByLogin(@PathVariable String login) {
		return userService.getUserByLogin(login);
	}

	@PutMapping("/user")
	public boolean updateUser(@RequestBody User updatedUser) {
		return userService.updateUser(updatedUser);
	}

	@DeleteMapping("/user/{id}")
	public boolean deleteUser(@PathVariable String id) {
		return userService.deleteUser(id);
	}

}
*/
