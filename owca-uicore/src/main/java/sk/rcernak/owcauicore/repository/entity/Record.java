package sk.rcernak.owcauicore.repository.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Record", uniqueConstraints = { @UniqueConstraint(columnNames = "RECORD_ID") })
public class Record implements Serializable {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "RECORD_ID", unique = true, nullable = false)
	private UUID recordId;

	@Column(name = "DATE", unique = true, nullable = false)
	private Date date;

	@Column(name = "TAG_ID", unique = false, nullable = false)
	private UUID tagId;

	public UUID getRecordId() {
		return recordId;
	}

	public void setRecordId(UUID recordId) {
		this.recordId = recordId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public UUID getTagId() {
		return tagId;
	}

	public void setTagId(UUID tagId) {
		this.tagId = tagId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Record))
			return false;
		Record record = (Record) o;
		return Objects.equals(recordId, record.recordId) && Objects.equals(date, record.date) && Objects.equals(tagId, record.tagId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(recordId, date, tagId);
	}
}
