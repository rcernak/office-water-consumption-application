package sk.rcernak.owcauicore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import sk.rcernak.owcauicore.repository.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
}

