package sk.rcernak.owcauicore;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import sk.rcernak.owcauicore.repository.UserRepository;
import sk.rcernak.owcauicore.repository.entity.User;

@RestController
@CrossOrigin()
@SpringBootApplication
public class OwcaUicoreApplication {

	@Autowired
	private UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(OwcaUicoreApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello() {
		final Iterable<User> all = userRepository.findAll();
		final List<UUID> users = new ArrayList<>();
		all.forEach(u -> users.add(u.getUserId()));
		return "Hello SpringBoot! " + users;
	}
}
