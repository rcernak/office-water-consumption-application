package sk.rcernak.owcauicore.security;

import java.util.HashMap;
import java.util.Map;

public enum AuthenticationCache {
	INSTANCE;

	private Map<String, String> alreadyAuthenticatedCache = new HashMap<>();


/*packagePrivate*/ void add(final String login, final String password) {
		if(alreadyAuthenticatedCache.size() > 100) {
			alreadyAuthenticatedCache.clear();
		}
		alreadyAuthenticatedCache.put(login, password);
	}

/*packagePrivate*/ boolean contains(final String login, final String password) {
		return alreadyAuthenticatedCache.containsKey(login) && alreadyAuthenticatedCache.get(login).contains(password);
	}

}
