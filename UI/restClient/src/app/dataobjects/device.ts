import { Record } from './record';

export class Device {
    id:string;
    name:string;
    volume:string;
    records:Record[];
}
