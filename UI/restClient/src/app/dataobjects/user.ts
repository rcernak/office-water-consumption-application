import { Device } from './device';

export class User {
    id:string;
    login:string;
    password:string;
    fname:string;
    lname:string;
    devices:Device[];
}
