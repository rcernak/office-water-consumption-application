export class Record {
    date:string;
    creatorId:string;
    tagId:string;
    userRegisteredDeviceName:string;
    volume:string;
}
