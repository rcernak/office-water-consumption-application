import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Constants } from '../constants';

export class UserAuthentication {
  constructor(
    public status:string,
     ) {}
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private httpClient:HttpClient) { }

  authenticate(username, password) {
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.httpClient.get<UserAuthentication>(Constants.API_ENDPOINT+'/validateLogin',{headers}).pipe(
      map(
        userData => {
         sessionStorage.setItem('username',username);
         let authString = 'Basic ' + btoa(username + ':' + password);
         sessionStorage.setItem('basicauth', authString);
         return userData;
        }
      )
 
     );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    return !(user === null)
  }

  isAdminLoggedIn() {
    let username = sessionStorage.getItem('username')
    return !(username === null) && (username == 'admin')
  }

  logOut() {
    sessionStorage.removeItem('username')
  }
}
