import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../dataobjects/user';
import { Constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private headers = new HttpHeaders({'Content-Type':'application/json'})
  private options = {headers:this.headers};
  private user = new User();
  

  constructor(private _http:HttpClient) { }

  getUsers() {
    return this._http.get(Constants.API_ENDPOINT+'/users');
  }

  getUser(id:string) {
    return this._http.get(Constants.API_ENDPOINT+'/user/'+id);
  }

  getUserByLogin(login:string) {
    return this._http.get(Constants.API_ENDPOINT+'/user/login/'+login);
  }

  deleteUser(id:string) {
    return this._http.delete(Constants.API_ENDPOINT+'/user/'+id);
  }

  createUser(user:User) {
    return this._http.post(Constants.API_ENDPOINT+'/user',JSON.stringify(user),this.options);
  }

  updateUser(user:User) {
    return this._http.put(Constants.API_ENDPOINT+'/user',JSON.stringify(user),this.options);
  }

  errorHandler(error:Response) {
    return Observable.throw(error||"SERVER ERROR");
  }

  setter(user:User) {
    this.user=user;
  }

  getter() {
    return this.user;
  }
}
