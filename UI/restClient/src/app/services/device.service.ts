import { Injectable } from '@angular/core';
import { Device } from '../dataobjects/device';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../constants';
@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  private device:Device;
  constructor(private _http:HttpClient) { }

  getUserTotal(userid:string) {
    return this._http.get(Constants.API_ENDPOINT+'/usertotal/'+userid);
  }

  getUserDevicesRecords(userid:string) {
    return this._http.get(Constants.API_ENDPOINT+'/userdevicesrecords/'+userid);
  }

  getUserDevices(userid:string) {
    return this._http.get(Constants.API_ENDPOINT+'/userdevices/'+userid);
  }
  
  setter(device:Device) {
    this.device=device;
  }

  getter() {
    return this.device;
  }


}
