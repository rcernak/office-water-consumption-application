import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { ListuserComponent } from './components/listuser/listuser.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { HeaderComponent } from './components/header/header.component'; 
import { AuthGuardService } from './services/auth-guard.service';
import { BasicAuthHttpInterceptorService } from './services/basic-auth-http-interceptor.service';
import { UserPageComponent } from './components/user-page/user-page.component';
import { DeviceFormComponent } from './components/device-form/device-form.component';

const appRoutes:Routes=[
  {path:'', component:ListuserComponent, canActivate:[AuthGuardService]},
  {path:'userform', component:UserFormComponent, canActivate:[AuthGuardService]},
  {path:'login', component:LoginComponent},
  {path:'logout', component:LogoutComponent, canActivate:[AuthGuardService]},
  {path:'user', component:UserPageComponent, canActivate:[AuthGuardService]},
  {path:'deviceform', component:DeviceFormComponent, canActivate:[AuthGuardService]}
];

@NgModule({
  declarations: [
    AppComponent,
    ListuserComponent,
    UserFormComponent,
    LoginComponent,
    LogoutComponent,
    HeaderComponent,
    UserPageComponent,
    DeviceFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  providers: [
    {
    provide:HTTP_INTERCEPTORS, useClass:BasicAuthHttpInterceptorService, multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
