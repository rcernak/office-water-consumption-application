import { Component, OnInit } from '@angular/core';
import { UserService} from '../../services/user.service';
import { User } from '../../dataobjects/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listuser',
  templateUrl: './listuser.component.html',
  styleUrls: ['./listuser.component.css']
})
export class ListuserComponent implements OnInit {

  public users:User[];

  constructor(private _userService:UserService, private _router:Router) { }

  ngOnInit() {
    this._userService.getUsers().subscribe((users)=>{
      console.log(users);
      this.users=<User[]>users;
    })
  }

  deleteUser(user:User) {
    this._userService.deleteUser(user.id).subscribe((data)=>{
      this.users.splice(this.users.indexOf(user),1)
    },(error)=>{console.log(error)});
  }

  updateUser(user:User) {
    this._userService.setter(user);
    this._router.navigate(['/userform']);
  }

  newUser() {
    let user = new User();
    this._userService.setter(user);
    this._router.navigate(['/userform']);
  }
}
