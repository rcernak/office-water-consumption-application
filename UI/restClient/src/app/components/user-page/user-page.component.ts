import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/dataobjects/user';
import { Device } from 'src/app/dataobjects/device';
import { DeviceService } from 'src/app/services/device.service';
import { Record } from 'src/app/dataobjects/record';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  public currentUser:User;
  public devices:Device[];
  private device:Device;
  public records:Record[];
  public total:string;


  constructor(private _userService:UserService, private _deviceService:DeviceService, private _router:Router) { }

  ngOnInit() {
    this.currentUser = this._userService.getter();
    this.setValues();
  }

  setValues() {
    this.setTotal().then(() => {
      this.setUserDevicesRecords().then(() => {
        this.setUserDevices().then(() => {
        });
      });
    });
  }
  
  setUserDevices() {
    return new Promise((resolved) => {
      this._deviceService.getUserDevices(this.currentUser.id).subscribe((devices:Device[])=>{
      this.devices=devices;
      });
      resolved();
    });
  }

  setUserDevicesRecords() {
    return new Promise((resolved) => {
      this._deviceService.getUserDevicesRecords(this.currentUser.id).subscribe((records:Record[])=>{
        this.records=records;
      });
      resolved(); 
    });
  }

  setTotal() {
    return new Promise((resolved) => {
      this._deviceService.getUserTotal(this.currentUser.id).subscribe((total:string)=>{
        this.total=total;        
      });
      resolved();
    });
  }

  newDevice() {
    this._userService.setter(this.currentUser);
    if(this.device==undefined) {
      this.device = new Device();
    }
    this._deviceService.setter(this.device);
    this._router.navigate(['/deviceform']);
  }

  updateDevice(device:Device) {
    this._userService.setter(this.currentUser);
    this._deviceService.setter(device);
    this._router.navigate(['/deviceform']);    
  }

  deleteDevice(device:Device) {
    var index = this.devices.indexOf(device);
    if (index > -1) {
      this.devices.splice(index, 1);
    }
    this.currentUser.devices=this.devices;
    this._userService.updateUser(this.currentUser).subscribe((data)=>{
      console.log(data);
    },(error)=>{console.log(error)});

  }
}
