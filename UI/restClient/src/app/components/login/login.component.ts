import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { error, debug } from 'util';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/dataobjects/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = 'testuser'
  password = 'testuser'
  invalidLogin = false

  constructor(private router: Router, 
    private loginService: AuthenticationService, private _userService: UserService) { }

  ngOnInit() {
  }

  checkLogin() {
    //TODO notify user about failed authentication and redirect to login page
    this.loginService.authenticate(this.username, this.password)
    .subscribe(
      data=> {
        var loggedUser:User;
        let authString:string = sessionStorage.getItem('username');

        new Promise((resolve) => {
          this._userService.getUserByLogin(authString).subscribe((user:User) => {
            loggedUser = user;
            resolve();
          });
      }).then(() => {
        this._userService.setter(loggedUser);
        this.router.navigate(['/user']);
      });
        this.invalidLogin = false
      },
      error=> {
        this.invalidLogin = true
      }
    );
  }

}
