import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/dataobjects/user';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { Device } from 'src/app/dataobjects/device';
import { DeviceService } from 'src/app/services/device.service';
import { debug } from 'util';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-device-form',
  templateUrl: './device-form.component.html',
  styleUrls: ['./device-form.component.css']
})
export class DeviceFormComponent implements OnInit {
  private user:User;
  public device:Device;

  constructor(private _userService:UserService, private _deviceService:DeviceService, private _router:Router) { }

  ngOnInit() {
    this.user=this._userService.getter();
    this.device=this._deviceService.getter();
  }

  processForm() {
    if(this.user.devices==undefined) {
      let devices = new Array<Device>();
      this.user.devices = devices;
    } 
    this._userService.updateUser(this.user).subscribe((user)=>{
      console.log(user);
      this._router.navigate(['/user']);
    },(error)=>{
      console.log(error);
    });
  }
}
