package com.ibm.sk.rcernak.raspberry.sub;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import com.ibm.sk.rcernak.FilesHelper;
import com.ibm.sk.rcernak.raspberry.WatsonIotProperties;

public class Main {

	@Deprecated
	public static void main(String[] args) throws MqttException {
		//MqttClient client=new MqttClient("tcp://localhost:1883", MqttClient.generateClientId());
		final WatsonIotProperties watsionIotProperties = FilesHelper.getWatsionIotProperties();

		MqttClient client=new MqttClient(watsionIotProperties.getServerUri(), watsionIotProperties.getClientId());
		final MqttConnectOptions options = new MqttConnectOptions();
		options.setUserName(watsionIotProperties.getUsername());
		options.setPassword(watsionIotProperties.getPassword().toCharArray());

		client.setCallback( new SimpleMqttCallBack() );
		client.connect(options);
		client.subscribe(watsionIotProperties.getTopic());
	}

}
