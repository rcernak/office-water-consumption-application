package com.ibm.sk.rcernak.raspberry.pub;

import static com.ibm.sk.rcernak.Constants.Raspberry.RASPBERRY_PI_DEFAULT_TAG_ID;
import static com.ibm.sk.rcernak.FilesHelper.getDeviceId;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.ibm.sk.rcernak.raspberry.pn532.WatsonIotPublisher;

public class Main {

	@Deprecated
	public static void main(String[] args) throws MqttException {
		final String deviceId = getDeviceId(args);
		final WatsonIotPublisher publisher = new WatsonIotPublisher();
		publisher.publish(RASPBERRY_PI_DEFAULT_TAG_ID, deviceId);
	}


}
