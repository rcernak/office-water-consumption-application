package com.ibm.sk.rcernak.raspberry.pn532;

public enum CommandStatus {
  OK, TIMEOUT, INVALID_ACK
}
