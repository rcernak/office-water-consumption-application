package com.ibm.sk.rcernak.raspberry.pn532;

import static com.ibm.sk.rcernak.Constants.Raspberry.DEVICE_ID;
import static com.ibm.sk.rcernak.Constants.Raspberry.TAG_ID;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ibm.sk.rcernak.FilesHelper;
import com.ibm.sk.rcernak.raspberry.WatsonIotProperties;

public class WatsonIotPublisher {

	private final Map<String, LocalTime> lastPersistTimeOfTag = new HashMap<>();

	public void publish(final String tagId, final String deviceId) throws MqttException {
		//MqttClient client = new MqttClient("tcp://localhost:1883", MqttClient.generateClientId());
		final WatsonIotProperties watsonIotProperties = FilesHelper.getWatsionIotProperties();

		final MqttClient client = new MqttClient(watsonIotProperties.getServerUri(), watsonIotProperties.getClientId());
		final MqttConnectOptions options = new MqttConnectOptions();
		options.setUserName(watsonIotProperties.getUsername());
		options.setPassword(watsonIotProperties.getPassword().toCharArray());
		client.connect(options);

		MqttMessage message = new MqttMessage();
		final JsonObject jsonObject = new JsonObject();
		addPropertyToJsonObject(jsonObject, DEVICE_ID, deviceId);
		addPropertyToJsonObject(jsonObject, TAG_ID, tagId);

		message.setPayload(jsonObject.toString().getBytes());

		client.publish(watsonIotProperties.getTopic(), message);
		client.disconnect();
	}

	private void addPropertyToJsonObject(final JsonObject jsonObject, final String key, final String value) {
		final JsonArray propertyValueArray = new JsonArray();
		propertyValueArray.add(value);
		jsonObject.add(key, propertyValueArray);
	}

	public void publishIfNecessary(final String tagId, final String deviceId) {
		final LocalTime currentPersistTime = LocalTime.now();
		LocalTime lastPersistTime = null;
		if(lastPersistTimeOfTag.containsKey(tagId)) {
			lastPersistTime = lastPersistTimeOfTag.get(tagId);
		}

		if(lastPersistTime == null || currentPersistTime.isAfter(lastPersistTime.plusSeconds(30))) {
			try {
				publish(tagId, deviceId);
			} catch (MqttException e) {
				System.out.println("Publish to WatsonIotPlatform not successful. deviceId: "+deviceId+", tagId: "+tagId+", exception: "+e.getMessage());
			}
			System.out.println("Tag " + tagId + " persisted at " + currentPersistTime);
			lastPersistTimeOfTag.put(tagId,currentPersistTime);
		} else {
			System.out.println("Tag " + tagId + " not persisted, because last persist was not more that 30 sec before:");
			System.out.println("Time now is: "+currentPersistTime+" last persist time was: "+lastPersistTime);

		}
	}
}
