package com.ibm.sk.rcernak.raspberry.pn532;

import static com.ibm.sk.rcernak.FilesHelper.getDeviceId;

public class Main {

	private static final byte PN532_MIFARE_ISO14443A = 0x00;

	private static WatsonIotPublisher publisher;

	public static void main(String[] args) throws InterruptedException {
		final String deviceId = getDeviceId(args);
		PN532 nfc = initPN532();

		if (nfc == null) {
			System.out.println("Could not initialize PN532 rfid reader");
			return;
		}

		System.out.println("Waiting for an ISO14443A Card ...");
		while (true) {
			final String rfidData = readRfidData(nfc);
			if(rfidData.isEmpty()) {
				continue;
			}
			publisher = publisher == null ? new WatsonIotPublisher() : publisher;
			publisher.publishIfNecessary(rfidData, deviceId);
		}
	}

	private static String readRfidData(PN532 nfc) throws InterruptedException {
		byte[] buffer = new byte[8];
		final StringBuilder stringBuffer = new StringBuilder();

		int readLength = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A,
				buffer);

		if (readLength > 0) {
			System.out.println("Found an ISO14443A card");

			System.out.print("  UID Length: ");
			System.out.print(readLength);
			System.out.println(" bytes");

			System.out.print("  UID Value: [");
			for (int i = 0; i < readLength; i++) {
				String value = Integer.toHexString(buffer[i]);
				System.out.print(value);
				stringBuffer.append(value);
			}
			System.out.println("]");
		}
		Thread.sleep(100);
		return stringBuffer.toString();
	}

	private static PN532 initPN532() throws InterruptedException {
		//		IPN532Interface pn532Interface = new PN532Spi();
		IPN532Interface pn532Interface = new PN532I2C();
		PN532 nfc = new PN532(pn532Interface);

		// Start
		System.out.println("Starting up...");
		nfc.begin();
		Thread.sleep(1000);

		long versiondata = nfc.getFirmwareVersion();
		if (versiondata == 0) {
			System.out.println("Didn't find PN53x board");
			return null;
		}
		// Got ok data, print it out!
		System.out.print("Found chip PN5");
		System.out.println(Long.toHexString((versiondata >> 24) & 0xFF));

		System.out.print("Firmware ver. ");
		System.out.print(Long.toHexString((versiondata >> 16) & 0xFF));
		System.out.print('.');
		System.out.println(Long.toHexString((versiondata >> 8) & 0xFF));

		// configure board to read RFID tags
		nfc.SAMConfig();
		return nfc;
	}

}