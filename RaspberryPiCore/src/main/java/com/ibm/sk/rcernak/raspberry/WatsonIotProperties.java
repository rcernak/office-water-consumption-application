package com.ibm.sk.rcernak.raspberry;

import java.util.Objects;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class WatsonIotProperties {

	public static class Builder {
		private String serverUri;
		private String clientId;
		private String username;
		private String password;
		private String topic;

		public WatsonIotProperties build() {
			return new WatsonIotProperties(this);
		}

		public Builder serverUri(String serverUri) {
			if(serverUri == null) {
				LOGGER.error("serverUri is null!");
			}
			this.serverUri = serverUri;
			return this;
		}

		public Builder clientId(String clientId) {
			if(clientId == null) {
				LOGGER.error("clientId is null!");
			}
			this.clientId = clientId;
			return this;
		}

		public Builder username(String username) {
			if(username == null) {
				LOGGER.error("username is null!");
			}
			this.username = username;
			return this;
		}

		public Builder password(String password) {
			if(password == null) {
				LOGGER.error("password is null!");
			}
			this.password = password;
			return this;
		}

		public Builder topic(String topic) {
			if(topic == null) {
				LOGGER.error("topic is null!");
			}
			this.topic = topic;
			return this;
		}

	}

	private final static Logger LOGGER = LogManager.getLogger(Builder.class);
	private final String serverUri;
	private final String clientId;
	private final String username;
	private final String password;
	private final String topic;

	private WatsonIotProperties(Builder builder) {
		this.serverUri = builder.serverUri;
		this.clientId = builder.clientId;
		this.username = builder.username;
		this.password = builder.password;
		this.topic = builder.topic;
	}

	public String getServerUri() {
		return serverUri;
	}

	public String getClientId() {
		return clientId;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getTopic() {
		return topic;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WatsonIotProperties))
			return false;
		WatsonIotProperties that = (WatsonIotProperties) o;
		return Objects.equals(serverUri, that.serverUri) && Objects.equals(clientId, that.clientId) && Objects
				.equals(username, that.username) && Objects.equals(password, that.password) && Objects.equals(topic, that.topic);
	}

	@Override
	public int hashCode() {
		return Objects.hash(serverUri, clientId, username, password, topic);
	}

}
