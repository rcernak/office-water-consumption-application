package com.ibm.sk.rcernak;

public class Constants {

	public class WatsonIot {
		public static final String WATSONIOT_PROPERTIES_FILE_NAME = "/watsoniot.properties";
	}

	public class Raspberry {
		public static final String RASPBERRY_PI_DEFAULT_ID = "raspberry_pi_default_id";
		public static final String RASPBERRY_PI_DEFAULT_TAG_ID = "1111";
		public static final String DEVICE_ID = "deviceId";
		public static final String TAG_ID = "tagId";
	}
}
