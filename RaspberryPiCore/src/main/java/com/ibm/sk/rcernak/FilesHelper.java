package com.ibm.sk.rcernak;

import static com.ibm.sk.rcernak.Constants.WatsonIot.WATSONIOT_PROPERTIES_FILE_NAME;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ibm.sk.rcernak.raspberry.WatsonIotProperties;

public class FilesHelper {

	private final static Logger LOGGER = LogManager.getLogger(FilesHelper.class);


	private FilesHelper() {
		//prevents instantiation of helper class
	}

	public static String getDeviceId(final String[] args) {
		return args.length > 0 ? args[0] : Constants.Raspberry.RASPBERRY_PI_DEFAULT_ID;
	}

	public static WatsonIotProperties getWatsionIotProperties() {
		final Properties watsonIotProperties = FilesHelper.getProperties(WATSONIOT_PROPERTIES_FILE_NAME);
		return new WatsonIotProperties.Builder()
				.serverUri(watsonIotProperties.getProperty("ServerUri"))
				.clientId(watsonIotProperties.getProperty("ClientId"))
				.username(watsonIotProperties.getProperty("Username"))
				.password(watsonIotProperties.getProperty("Password"))
				.topic(watsonIotProperties.getProperty("Topic"))
				.build();
	}

	public static Properties getProperties(final String fileName) {
		Properties props = new Properties();
		try {
			final InputStream resourceAsStream = FilesHelper.class.getResourceAsStream(fileName);
			props.load(resourceAsStream);
		} catch (IOException e1) {
			LOGGER.error("Not able to read the properties file, exiting..");
			System.exit(-1);
		}
		return props;
	}
}
