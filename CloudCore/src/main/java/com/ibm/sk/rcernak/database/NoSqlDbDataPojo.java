package com.ibm.sk.rcernak.database;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

public class NoSqlDbDataPojo {

	private UUID id;
	private String deviceId;
	private String deviceType;
	private String event;
	private Object data;
	private String format;
	private LocalDateTime date;
	private byte[] rawPayload;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public byte[] getRawPayload() {
		return rawPayload;
	}

	public void setRawPayload(byte[] rawPayload) {
		this.rawPayload = rawPayload;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public LocalDateTime getDate() {
		return date;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof NoSqlDbDataPojo))
			return false;
		NoSqlDbDataPojo that = (NoSqlDbDataPojo) o;
		return Objects.equals(id, that.id) && Objects.equals(deviceId, that.deviceId) && Objects.equals(deviceType, that.deviceType)
				&& Objects.equals(event, that.event) && Objects.equals(data, that.data) && Objects.equals(format, that.format) && Objects
				.equals(date, that.date) && Arrays.equals(rawPayload, that.rawPayload);
	}

	@Override
	public int hashCode() {
		int result = Objects.hash(id, deviceId, deviceType, event, data, format, date);
		result = 31 * result + Arrays.hashCode(rawPayload);
		return result;
	}

	@Override
	public String toString() {
		return "NoSqlDbDataPojo{" + "id=" + id + ", deviceId='" + deviceId + '\'' + ", deviceType='" + deviceType + '\'' + ", event='" + event
				+ '\'' + ", data=" + data + ", format='" + format + '\'' + ", date=" + date + ", rawPayload=" + Arrays.toString(rawPayload)
				+ '}';
	}
}
