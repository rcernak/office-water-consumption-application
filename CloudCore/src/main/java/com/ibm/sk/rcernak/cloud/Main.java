package com.ibm.sk.rcernak.cloud;

import static com.ibm.sk.rcernak.Constants.ClaudantDb.CLAUDANT_PROPERTIES_FILE_NAME;
import static com.ibm.sk.rcernak.Constants.WatsonIot.WATSONIOT_PROPERTIES_FILE_NAME;
import static com.ibm.sk.rcernak.FilesHelper.getProperties;

import com.ibm.sk.rcernak.FilesHelper;
import com.ibm.sk.rcernak.database.ClaudantDb;

public class Main {

	public static void main(String[] args) throws Exception {
		final ClaudantDb claudantDb = new ClaudantDb(getProperties(CLAUDANT_PROPERTIES_FILE_NAME));
		//testWriteAndReadFromDb(raspberryNoSqlDb);
		WatsonIotSubscription.createEventSubscription(FilesHelper.getProperties(WATSONIOT_PROPERTIES_FILE_NAME), new WatsonIotSubscription.MyEventCallback(claudantDb));
	}

}
