package com.ibm.sk.rcernak;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ibm.sk.rcernak.cloud.WatsonIotSubscription;

public class FilesHelper {

	private final static Logger LOGGER = LogManager.getLogger(FilesHelper.class);


	private FilesHelper() {
		//prevents instantiation of helper class
	}

	public static Properties getProperties(final String fileName) {
		Properties props = new Properties();
		try {
			final InputStream resourceAsStream = WatsonIotSubscription.class.getResourceAsStream(fileName);
			props.load(resourceAsStream);
		} catch (IOException e1) {
			LOGGER.error("Not able to read the properties file, exiting..");
			System.exit(-1);
		}
		return props;
	}
}
