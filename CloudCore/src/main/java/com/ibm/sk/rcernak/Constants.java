package com.ibm.sk.rcernak;

public class Constants {

	public class WatsonIot {
		public static final String WATSONIOT_PROPERTIES_FILE_NAME = "/watsoniot.properties";
	}

	public class ClaudantDb {
		public static final String CLAUDANT_PROPERTIES_FILE_NAME = "/claudant.properties";
	}
}
