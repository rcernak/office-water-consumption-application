package com.ibm.sk.rcernak.cloud;

import java.time.LocalDateTime;
import java.util.*;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.cloudant.client.api.model.Response;
import com.ibm.iotf.client.app.ApplicationClient;
import com.ibm.iotf.client.app.Command;
import com.ibm.iotf.client.app.Event;
import com.ibm.iotf.client.app.EventCallback;
import com.ibm.sk.rcernak.database.ClaudantDb;
import com.ibm.sk.rcernak.database.NoSqlDbDataPojo;

public class WatsonIotSubscription {

	private final static Logger LOGGER = LogManager.getLogger(WatsonIotSubscription.class);

	public static void createEventSubscription(Properties watsonIotProperties, MyEventCallback myEventCallback) {
		ApplicationClient myAppClient = null;
		try {
			//Instantiate the class by passing the properties file
			myAppClient = new ApplicationClient(watsonIotProperties);
			myAppClient.connect();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		myAppClient.setEventCallback(myEventCallback);
		myAppClient.subscribeToDeviceEvents();
	}

	public static class MyEventCallback implements EventCallback {

		private final ClaudantDb noSqlDb;

		public MyEventCallback(final ClaudantDb noSqlDb) {
			this.noSqlDb = noSqlDb;
		}

		public void processEvent(Event e) {
			NoSqlDbDataPojo dbData = parseEventData(e);
			final Response response = noSqlDb.write(dbData);

			if(response == null || response.getId() == null) {
				LOGGER.info("Data could not be read from database. Problem with response: " + response);
				return;
			}
			LOGGER.info("Data read from database: "+ noSqlDb.read(response.getId()));
		}

		public void processCommand(Command cmd) {
			LOGGER.info("Command " + cmd.getPayload());
		}
	}

	//TODO builder
	private static NoSqlDbDataPojo parseEventData(Event e) {
		final NoSqlDbDataPojo dbData = new NoSqlDbDataPojo();
		dbData.setId(UUID.randomUUID());
		dbData.setDeviceId(e.getDeviceId());
		dbData.setEvent(e.getEvent());
		dbData.setData(e.getData());
		dbData.setDeviceType(e.getDeviceType());
		dbData.setFormat(e.getFormat());
		dbData.setRawPayload(e.getRawPayload());
		dbData.setDate(LocalDateTime.now());

		return dbData;
	}

}
