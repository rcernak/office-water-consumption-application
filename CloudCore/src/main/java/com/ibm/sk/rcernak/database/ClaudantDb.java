package com.ibm.sk.rcernak.database;


import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.cloudant.client.api.model.Response;

public class ClaudantDb {

	private final static Logger LOGGER = LogManager.getLogger(ClaudantDb.class);

	private final Database database;

	public Database getDatabase() {
		return database;
	}

	public ClaudantDb(final Properties claudantProperties) throws MalformedURLException {
		final String url = claudantProperties.getProperty("url");
		final String username = claudantProperties.getProperty("username");
		final String password = claudantProperties.getProperty("password");
		final String databaseName = claudantProperties.getProperty("database");

		final CloudantClient cloudantClient = ClientBuilder.url(new URL(url)).username(username).password(password).build();

		this.database = cloudantClient.database(databaseName, false);
	}

	private void testWriteAndReadFromDb() throws IOException {
		final String testId = "testId";
		if(!database.contains(testId)) {
			Map<String, Object> data = new HashMap<>();
			data.put("name", "testName");
			data.put("_id", testId);
			data.put("value", "testValue");
			data.put("creation_date", new Date().toString());
			database.save(data);
		}

		final InputStreamReader reader = new InputStreamReader(database.find(testId));
		int character;
		StringBuilder sb = new StringBuilder();
		while((character = reader.read())!=-1) {
			sb.append((char)character);
		}
		LOGGER.info(sb);
	}

	public NoSqlDbDataPojo read(final String id) {
		if(!database.contains(id)) {
			LOGGER.info("No data with this ID: " + id);
			return null;
		}
		return database.find(NoSqlDbDataPojo.class, id);
	}

	public Response write(NoSqlDbDataPojo dbData) {
		final String dbEntryId = dbData.getId().toString();

		if(database.contains(dbEntryId)) {
			LOGGER.info("Data with this id already in database");
			return null;
		}

		final Response response = database.save(dbData);
		LOGGER.info("Data successfully written to database: ");
		LOGGER.info(dbData);
		return response;
	}
}
